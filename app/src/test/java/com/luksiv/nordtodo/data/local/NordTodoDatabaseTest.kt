package com.luksiv.nordtodo.data.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.luksiv.nordtodo.data.local.todos.Todo
import com.luksiv.nordtodo.data.local.todos.TodoRepository
import com.luksiv.nordtodo.helpers.getOrAwaitValue
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config


@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28])
class NordTodoDatabaseTest {

    // FOR DATA
    private lateinit var database: NordTodoDatabase

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUpDatabase() {
        database = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().targetContext,
            NordTodoDatabase::class.java
        ).allowMainThreadQueries()
            .build()
    }

    @After
    fun closeDatabase() {
        database.close()
    }

    @Test
    fun test_insert() {
        val todo = createTodo(1)

        database.todoDao().insertTodo(todo).blockingAwait()

        val todos = database.todoDao().fetchAllTodos().blockingGet()

        assert(todos.size == 1) { "Todo list size must be 1" }
        assert(todos.first() == todo) { "Database todo must match inserted todo" }
    }

    @Test
    fun test_fetch_all() {
        val prefillSize = 100

        prefillDatabase(prefillSize)

        val todos = database.todoDao().fetchAllTodos().blockingGet()

        assert(todos.size == prefillSize) { "Todo list size must be $prefillSize (now is: ${todos.size})" }
        todos.forEachIndexed { index, todo ->
            assert(todo.id == index + 1) { "Database todo index must match inserted todo returned index" }
        }
    }

    @Test
    fun test_fetch_all_ids() {
        val prefillSize = 100

        prefillDatabase(prefillSize)

        val ids = database.todoDao().fetchAllIds().blockingGet()

        assert(ids.size == prefillSize) { "Todo list size must be $prefillSize (now is: ${ids.size})" }
        ids.forEachIndexed { index, id ->
            assert(id == index + 1) { "Database todo index must match inserted todo returned index" }
        }
    }

    @Test
    fun test_update() {
        val prefillSize = 10

        prefillDatabase(prefillSize)

        val newTodo = createTodo(1, 2, "test", 9, 9, true)

        database.todoDao().updateTodo(newTodo).blockingAwait()

        val updatedTodo = database.todoDao().fetchTodo(1).getOrAwaitValue()

        assert(updatedTodo != null) { "Todo must exist" }

        assert(updatedTodo == newTodo) { "Updated todo must match all parameters" }
    }

    @Test
    fun test_fetch_one_success() {
        val prefillSize = 10

        prefillDatabase(prefillSize)

        val todo = database.todoDao().fetchTodo(1).getOrAwaitValue()

        assert(todo != null) { "Todo must exist" }
        assert(todo == createTodo(1)) { "Todo must match all parameters" }
    }

    @Test
    fun test_fetch_one_fail() {
        val prefillSize = 10

        prefillDatabase(prefillSize)

        val todo = database.todoDao().fetchTodo(prefillSize + 1).getOrAwaitValue()

        assert(todo == null) { "Todo must not exist" }
    }

    @Test
    fun test_delete_todo() {
        val prefillSize = 10

        prefillDatabase(prefillSize)

        database.todoDao().deleteTodo(createTodo(1))

        val todo = database.todoDao().fetchTodo(prefillSize + 1).getOrAwaitValue()

        assert(todo == null) { "Todo must not exist" }
    }

    @Test
    fun test_delete_batch_todos() {
        val prefillSize = 100

        prefillDatabase(prefillSize)

        val ids = database.todoDao().fetchAllIds().blockingGet()

        database.todoDao().deleteTodos(ids).blockingAwait()

        val todos = database.todoDao().fetchAllIds().blockingGet()

        assert(todos.isEmpty()) { "Todos must be empty (current size: ${todos.size})" }
    }

    @Test
    fun test_repository_delete_batch_todos() {
        val prefillSize = 500

        prefillDatabase(prefillSize)

        val todoRepository = TodoRepository(database.todoDao())

        todoRepository.deleteInvalidTodos(emptyList()).blockingAwait()

        val todos = database.todoDao().fetchAllIds().blockingGet()

        assert(todos.isEmpty()) { "Todos must be empty (current size: ${todos.size})" }
    }

    @Test
    fun test_repository_delete_batch_all_valid_todos() {
        val prefillSize = 500

        prefillDatabase(prefillSize)

        val todoRepository = TodoRepository(database.todoDao())

        val validIds = database.todoDao().fetchAllIds().blockingGet()

        todoRepository.deleteInvalidTodos(validIds).blockingAwait()

        val todos = database.todoDao().fetchAllIds().blockingGet()

        assert(todos.size == prefillSize) { "Todos count must match prefill size (current size: ${todos.size})" }
    }

    @Test
    fun test_repository_delete_batch_todos_sqlite_overflow() {
        val prefillSize = 2500

        prefillDatabase(prefillSize)

        val todoRepository = TodoRepository(database.todoDao())

        todoRepository.deleteInvalidTodos(emptyList()).blockingAwait()

        val todos = database.todoDao().fetchAllIds().blockingGet()

        assert(todos.isEmpty()) { "Todos must be empty (current size: ${todos.size})" }
    }

    @Test
    fun test_repository_delete_batch_todos_all_valid_sqlite_overflow() {
        val prefillSize = 2500

        prefillDatabase(prefillSize)

        val validIds = database.todoDao().fetchAllIds().blockingGet()

        val todoRepository = TodoRepository(database.todoDao())

        todoRepository.deleteInvalidTodos(validIds).blockingAwait()

        val todos = database.todoDao().fetchAllIds().blockingGet()

        assert(todos.size == prefillSize) { "Todos count must match prefill size (current size: ${todos.size})" }
    }

    private fun prefillDatabase(amount: Int) {
        for(i in 1 .. amount) {
            database.todoDao().insertTodo(createTodo(i)).blockingAwait()
        }
    }

    private fun createTodo(
        id: Int,
        userId: Int = 1,
        title: String = "Test todo #$id",
        createdAt: Long = 1,
        updatedAt: Long = 1,
        isCompleted: Boolean = false
    ): Todo {
        return Todo(id, userId, title, createdAt, updatedAt, isCompleted)
    }
}