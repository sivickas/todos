package com.luksiv.nordtodo.features.todos.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.luksiv.nordtodo.data.local.todos.Todo
import com.luksiv.nordtodo.data.local.todos.TodoRepository
import io.mockk.every
import io.mockk.mockk
import org.junit.Rule
import org.junit.Test

class TodoDetailsViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val todoRepository: TodoRepository = mockk()
    private val todoDetailsViewModel = TodoDetailsViewModel(todoRepository)

    @Test
    fun test_todo_synchronization_success() {
        val todo = Todo(1, 1, "Test todo #1", 1, 2, false)
        every { todoRepository.fetchTodo(1) } returns MutableLiveData(todo)

        assert(todoDetailsViewModel.fetchTodo(1).value == todo) { "ViewModel todo LiveData is not the same as mocked" }
    }

    @Test
    fun test_todo_synchronization_failure() {
        every { todoRepository.fetchTodo(1) } returns MutableLiveData(null)

        assert(todoDetailsViewModel.fetchTodo(1).value == null) { "ViewModel todo LiveData is not the same as mocked" }
    }
}