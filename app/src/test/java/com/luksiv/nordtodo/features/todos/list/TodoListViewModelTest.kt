package com.luksiv.nordtodo.features.todos.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.luksiv.nordtodo.core.entities.SynchronizationStatus
import com.luksiv.nordtodo.core.rx.SchedulerProvider
import com.luksiv.nordtodo.data.local.todos.Todo
import com.luksiv.nordtodo.data.local.todos.TodoRepository
import com.luksiv.nordtodo.data.synchronization.TodoSynchronizationManager
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.mockk.verifyAll
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.net.ConnectException
import java.net.UnknownHostException

class TodoListViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testScheduler = TestScheduler()
    private lateinit var schedulerProvider: SchedulerProvider

    private val todoRepository: TodoRepository = mockk()
    private val todoSynchronizationManager: TodoSynchronizationManager = mockk()

    @Before
    fun setUp() {
        schedulerProvider = object : SchedulerProvider {
            override fun mainScheduler() = testScheduler
            override fun ioScheduler() = testScheduler
        }
    }

    @Test
    fun test_todo_synchronization_success() {
        every { todoSynchronizationManager.synchronize() } returns Observable.create {
            it.onComplete()
        }

        val todos = listOf(
            Todo(1, 1, "Test todo #1", 1, 2, false),
            Todo(2, 2, "Test todo #2", 2, 2, true),
            Todo(3, 3, "Test todo #3", 3, 3, false),
            Todo(4, 1, "Test todo #4", 4, 5, true),
        )

        every { todoRepository.fetchAllTodos() } returns Single.just(todos)

        var todoListViewModel = TodoListViewModel(
            todoRepository,
            todoSynchronizationManager,
            schedulerProvider
        )

        todoListViewModel.synchronizeTodos()

        testScheduler.triggerActions()

        verifyAll {
            todoSynchronizationManager.synchronize()
            todoRepository.fetchAllTodos()
        }

        assert(todos == todoListViewModel.todos.value) { "ViewModel todos LiveData is not the same as mocked list" }

        assert(todoListViewModel.synchronizationStatus.value == SynchronizationStatus.Completed) { "Synchronization status must be set to completed" }
    }

    @Test
    fun test_todo_synchronization_error_no_internet() {
        every { todoSynchronizationManager.synchronize() } returns Observable.create {
            it.onError(UnknownHostException("test.host.lt"))
        }

        var todoListViewModel = TodoListViewModel(
            todoRepository,
            todoSynchronizationManager,
            schedulerProvider
        )

        todoListViewModel.synchronizeTodos()

        testScheduler.triggerActions()

        verify {
            todoSynchronizationManager.synchronize()
        }

        assert(todoListViewModel.todos.value == null) { "ViewModel todos LiveData must be null" }
        assert(todoListViewModel.synchronizationStatus.value == SynchronizationStatus.SynchronizationError.NoInternetError) { "Synchronization status must be no internet error" }
    }

    @Test
    fun test_todo_synchronization_error_connection_lost() {
        every { todoSynchronizationManager.synchronize() } returns Observable.create {
            it.onError(ConnectException("this is a test"))
        }

        var todoListViewModel = TodoListViewModel(
            todoRepository,
            todoSynchronizationManager,
            schedulerProvider
        )

        todoListViewModel.synchronizeTodos()

        testScheduler.triggerActions()

        verify {
            todoSynchronizationManager.synchronize()
        }

        assert(todoListViewModel.todos.value == null) { "ViewModel todos LiveData must be null" }
        assert(todoListViewModel.synchronizationStatus.value == SynchronizationStatus.SynchronizationError.InternetConnectionLostError) { "Synchronization status must be internet connection lost" }
    }

    @Test
    fun test_todo_synchronization_error_unknown() {
        val unknownErrorMessage = "this is a very spooky unknown error"

        every { todoSynchronizationManager.synchronize() } returns Observable.create {
            it.onError(Error(unknownErrorMessage))
        }

        var todoListViewModel = TodoListViewModel(
            todoRepository,
            todoSynchronizationManager,
            schedulerProvider
        )

        todoListViewModel.synchronizeTodos()

        testScheduler.triggerActions()

        verify {
            todoSynchronizationManager.synchronize()
        }

        assert(todoListViewModel.todos.value == null) { "ViewModel todos LiveData must be null" }
        assert(todoListViewModel.synchronizationStatus.value is SynchronizationStatus.SynchronizationError.UnknownError) { "Synchronization status must be internet connection lost" }
        assert((todoListViewModel.synchronizationStatus.value as SynchronizationStatus.SynchronizationError.UnknownError).error.message == unknownErrorMessage) { "Synchronization status must be internet connection lost" }
    }

    @Test
    fun test_fetching_todos_success() {
        every { todoSynchronizationManager.synchronize() } returns Observable.create {
            it.onComplete()
        }

        var todoListViewModel = TodoListViewModel(
            todoRepository,
            todoSynchronizationManager,
            schedulerProvider
        )

        val todos = listOf(
            Todo(1, 1, "Test todo #1", 1, 2, false),
            Todo(2, 2, "Test todo #2", 2, 2, true),
            Todo(3, 3, "Test todo #3", 3, 3, false),
            Todo(4, 1, "Test todo #4", 4, 5, true),
        )

        every { todoRepository.fetchAllTodos() } returns Single.just(todos)

        todoListViewModel.fetchTodos()

        testScheduler.triggerActions()

        assert(todos == todoListViewModel.todos.value) { "ViewModel todos LiveData is not the same as mocked list" }
    }
}