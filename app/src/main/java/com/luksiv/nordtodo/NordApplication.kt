package com.luksiv.nordtodo

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class NordApplication : Application() {

    override fun onCreate() {

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        Timber.d("Application created")

        super.onCreate()
    }
}