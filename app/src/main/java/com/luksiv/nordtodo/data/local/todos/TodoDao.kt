package com.luksiv.nordtodo.data.local.todos

import androidx.lifecycle.LiveData
import androidx.room.*
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

@Dao
interface TodoDao {

    @Query("SELECT id FROM todos")
    fun fetchAllIds(): Single<List<Int>>

    @Query("SELECT * FROM todos ORDER BY created_at DESC ")
    fun fetchAllTodos(): Single<List<Todo>>

    @Insert
    fun insertTodo(todo: Todo): Completable

    @Query("SELECT * FROM todos WHERE id = :id")
    fun fetchTodo(id: Int): LiveData<Todo?>

    @Update
    fun updateTodo(todo: Todo): Completable

    @Delete
    fun deleteTodo(todo: Todo): Completable

    @Query("DELETE FROM todos WHERE id in (:ids) ")
    fun deleteTodos(ids: List<Int>): Completable
}