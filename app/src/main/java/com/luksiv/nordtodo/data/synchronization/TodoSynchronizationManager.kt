package com.luksiv.nordtodo.data.synchronization

import com.luksiv.nordtodo.core.rx.SchedulerProvider
import com.luksiv.nordtodo.data.local.todos.Todo
import com.luksiv.nordtodo.data.local.todos.TodoRepository
import com.luksiv.nordtodo.data.network.todos.TodoApiClient
import com.luksiv.nordtodo.data.network.todos.entities.TodosApiResponse
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.kotlin.toObservable
import timber.log.Timber

class TodoSynchronizationManager(
    private val todoApiClient: TodoApiClient,
    private val todoRepository: TodoRepository,
    private val schedulerProvider: SchedulerProvider,
) {

    /**
     * Synchronizes all todos with the backend while returning synchronization progress (0 - 100)
     */
    fun synchronize(): Observable<Int> {
        return Observable.create { emitter ->
            emitter.onNext(0)

            val validIds = mutableListOf<Int>()

            getPageAndNext(1)
                .subscribeOn(schedulerProvider.ioScheduler())
                .doOnNext {
                    val page = it.metadata.pagination.page
                    val pages = it.metadata.pagination.pages
                    val progress = (page * 100f / pages).toInt()

                    emitter.onNext(progress)
                }
                .flatMap {
                    it.todos.toObservable()
                }
                .flatMapCompletable {
                    val todo = Todo(
                        it.id,
                        it.userId,
                        it.title,
                        it.createdAt.time,
                        it.updatedAt.time,
                        it.completed
                    )

                    validIds.add(it.id)

                    todoRepository.insertTodo(todo)
                        .onErrorResumeNext {
                            todoRepository.updateTodo(todo)
                        }
                }.andThen { observer ->
                    todoRepository.deleteInvalidTodos(validIds)
                        .subscribeBy(
                            onComplete = { observer.onComplete() },
                            onError = { observer.onError(it) }
                        )
                }.subscribeBy(
                    onComplete = {
                        emitter.onComplete()
                    },
                    onError = {
                        Timber.e(it)
                        emitter.onError(it)
                    }
                )
        }
    }

    /**
     * Gets the pages data and if there is another page after it, calls itself for that page recursively
     * @param page Page from which to get the information
     */
    private fun getPageAndNext(page: Int): Observable<TodosApiResponse> {
        return todoApiClient.getTodos(page)
            .toObservable()
            .concatMap {
                val paginationInfo = it.metadata.pagination
                if (paginationInfo.page == paginationInfo.pages) {
                    Observable.just(it)
                } else {
                    Observable.just(it)
                        .concatWith(getPageAndNext(page + 1))
                }
            }
    }
}