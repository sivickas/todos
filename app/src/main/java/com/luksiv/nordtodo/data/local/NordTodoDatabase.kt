package com.luksiv.nordtodo.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.luksiv.nordtodo.data.local.todos.Todo
import com.luksiv.nordtodo.data.local.todos.TodoDao

@Database(
    entities = [
        Todo::class
    ],
    version = 1,
    exportSchema = true
)
abstract class NordTodoDatabase : RoomDatabase() {

    abstract fun todoDao(): TodoDao
}