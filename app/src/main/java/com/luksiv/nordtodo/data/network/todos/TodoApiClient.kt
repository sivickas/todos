package com.luksiv.nordtodo.data.network.todos

class TodoApiClient(private val todoService: TodoService) {
    fun getTodos(page: Int) = todoService.getTodos(page)
}