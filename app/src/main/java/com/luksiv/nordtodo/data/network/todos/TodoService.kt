package com.luksiv.nordtodo.data.network.todos

import com.luksiv.nordtodo.data.network.todos.entities.TodosApiResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface TodoService {
    @GET("todos")
    fun getTodos(@Query("page") page: Int): Single<TodosApiResponse>
}