package com.luksiv.nordtodo.data.network.todos.entities


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class TodosApiResponse(
    @Json(name = "code")
    val responseCode: Int,
    @Json(name = "data")
    val todos: List<ApiTodo>,
    @Json(name = "meta")
    val metadata: Metadata
) {
    @JsonClass(generateAdapter = true)
    data class ApiTodo(
        val id: Int,
        @Json(name = "user_id")
        val userId: Int,
        val title: String,
        val completed: Boolean,
        @Json(name = "created_at")
        val createdAt: Date,
        @Json(name = "updated_at")
        val updatedAt: Date
    )

    @JsonClass(generateAdapter = true)
    data class Metadata(
        val pagination: Pagination
    ) {
        @JsonClass(generateAdapter = true)
        data class Pagination(
            val limit: Int,
            val page: Int,
            val pages: Int,
            val total: Int
        )
    }
}