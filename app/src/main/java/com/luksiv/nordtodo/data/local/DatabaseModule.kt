package com.luksiv.nordtodo.data.local

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideNordTodoDatabase(
        @ApplicationContext applicationContext: Context
    ): NordTodoDatabase {
        return Room
            .databaseBuilder(
                applicationContext,
                NordTodoDatabase::class.java,
                "NordTodoDatabase"
            )
            .build()
    }
}