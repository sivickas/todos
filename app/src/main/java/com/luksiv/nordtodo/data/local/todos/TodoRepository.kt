package com.luksiv.nordtodo.data.local.todos

import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import kotlin.math.floor
import kotlin.math.min

class TodoRepository(
    private val todoDao: TodoDao
) {

    fun fetchAllTodos() = todoDao.fetchAllTodos()

    fun insertTodo(todo: Todo) = todoDao.insertTodo(todo)

    fun fetchTodo(todoId: Int) = todoDao.fetchTodo(todoId)

    fun updateTodo(todo: Todo) = todoDao.updateTodo(todo)

    fun deleteTodo(todo: Todo) = todoDao.deleteTodo(todo)

    /**
     * Deletes all todos in the database which are not in the validIds list
     *
     * @param validIds t0do ids that are valid
     */
    fun deleteInvalidTodos(validIds: List<Int>): Completable {
        return todoDao
            .fetchAllIds()
            .flatMapObservable { localIds ->
                val deletedTodoIds = localIds - validIds

                // To reduce the amount of SQL calls, we will delete them using `in` parameter,
                // but SQLITE only allows the maximum of 999 query variables,
                // so that's why we're batching them

                val batches = mutableListOf<List<Int>>()

                if (deletedTodoIds.isNotEmpty()) {
                    for (i in 0..floor(deletedTodoIds.size / 999f).toInt()) {
                        val from = i * 999
                        val to = min(deletedTodoIds.size, (i + 1) * 999)
                        batches.add(deletedTodoIds.subList(from, to))
                    }
                }

                Observable.fromIterable(batches)
            }.flatMapCompletable {
                todoDao.deleteTodos(it)
            }
    }
}