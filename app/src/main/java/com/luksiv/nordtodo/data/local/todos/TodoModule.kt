package com.luksiv.nordtodo.data.local.todos

import com.luksiv.nordtodo.data.local.NordTodoDatabase
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object TodoModule {

    @Provides
    @Singleton
    fun provideTodoDao(nordTodoDatabase: NordTodoDatabase): TodoDao {
        return nordTodoDatabase.todoDao()
    }

    @Provides
    @Reusable
    fun provideTodoRepository(todoDao: TodoDao): TodoRepository {
        return TodoRepository(todoDao)
    }
}