package com.luksiv.nordtodo.data.synchronization

import com.luksiv.nordtodo.core.rx.SchedulerProvider
import com.luksiv.nordtodo.data.local.todos.TodoRepository
import com.luksiv.nordtodo.data.network.todos.TodoApiClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object SynchronizationModule {

    @Provides
    @Singleton
    fun provideTodoSynchronizationManager(
        todoApiClient: TodoApiClient,
        todoRepository: TodoRepository,
        schedulerProvider: SchedulerProvider
    ): TodoSynchronizationManager {
        return TodoSynchronizationManager(
            todoApiClient,
            todoRepository,
            schedulerProvider,
        )
    }
}