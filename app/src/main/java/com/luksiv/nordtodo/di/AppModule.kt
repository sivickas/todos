package com.luksiv.nordtodo.di

import com.luksiv.nordtodo.core.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    @Named("io")
    fun provideIoScheduler(): Scheduler = Schedulers.io()

    @Singleton
    @Provides
    @Named("main")
    fun provideMainScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Singleton
    @Provides
    fun provideSchedulerProvider(
        @Named("main") mainScheduler: Scheduler,
        @Named("io") ioScheduler: Scheduler
    ): SchedulerProvider {
        return object : SchedulerProvider {
            override fun mainScheduler(): Scheduler = mainScheduler
            override fun ioScheduler(): Scheduler = ioScheduler
        }
    }
}