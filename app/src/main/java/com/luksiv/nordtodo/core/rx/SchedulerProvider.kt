package com.luksiv.nordtodo.core.rx

import io.reactivex.rxjava3.core.Scheduler

/**
 * Provides schedulers for main and io thread work
 */
interface SchedulerProvider {
    fun mainScheduler(): Scheduler
    fun ioScheduler(): Scheduler
}