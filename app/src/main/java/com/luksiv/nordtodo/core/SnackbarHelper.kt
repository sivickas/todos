package com.luksiv.nordtodo.core

import android.view.View
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.luksiv.nordtodo.R

object SnackbarHelper {

    fun showErrorSnackbar(
        view: View,
        @StringRes errorMessage: Int,
        duration: Int = Snackbar.LENGTH_LONG,
        retryAction: (() -> Unit)? = null
    ) {
        Snackbar.make(view, errorMessage, duration).apply {

            if (retryAction != null) {
                setAction(R.string.action_retry) {
                    retryAction()
                }
            }

            setTextColor(ContextCompat.getColor(context, R.color.main_white))
            setActionTextColor(ContextCompat.getColor(context, R.color.main_white))
            setBackgroundTint(ContextCompat.getColor(context, R.color.rose_red))

            show()
        }
    }
}