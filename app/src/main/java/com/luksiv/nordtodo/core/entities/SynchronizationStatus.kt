package com.luksiv.nordtodo.core.entities

sealed class SynchronizationStatus {
    class Synchronizing(val progress: Int) : SynchronizationStatus()
    object Completed : SynchronizationStatus()
    sealed class SynchronizationError : SynchronizationStatus() {
        object NoInternetError : SynchronizationError()
        object InternetConnectionLostError : SynchronizationError()
        class UnknownError(val error: Throwable) : SynchronizationError()
    }
}