package com.luksiv.nordtodo.core.extensions

import java.util.*

inline fun runDelayed(delayMillis: Long, crossinline action: () -> Unit) {
    Timer().schedule(object : TimerTask() {
        override fun run() {
            action()
        }
    }, delayMillis)
}