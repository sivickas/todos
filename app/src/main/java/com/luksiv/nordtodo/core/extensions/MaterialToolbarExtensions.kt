package com.luksiv.nordtodo.core.extensions

import androidx.navigation.findNavController
import com.google.android.material.appbar.MaterialToolbar
import com.luksiv.nordtodo.R

fun MaterialToolbar.enableBackNavigation() {
    setNavigationIcon(R.drawable.ic_navigation_arrow_back)
    setNavigationOnClickListener {
        findNavController().navigateUp()
    }
}