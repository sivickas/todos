package com.luksiv.nordtodo.core.extensions

import android.view.View
import androidx.annotation.StringRes

fun View.getString(@StringRes resId: Int): String = context.getString(resId)