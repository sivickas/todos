package com.luksiv.nordtodo.core.extensions

import android.graphics.Paint
import android.widget.TextView

fun TextView.setStrikeThrough(isStrikeThrough: Boolean) {
    paintFlags = if (isStrikeThrough) {
        paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
    } else {
        paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
    }
}