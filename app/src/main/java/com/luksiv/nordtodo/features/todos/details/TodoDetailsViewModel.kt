package com.luksiv.nordtodo.features.todos.details

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.luksiv.nordtodo.data.local.todos.Todo
import com.luksiv.nordtodo.data.local.todos.TodoRepository

class TodoDetailsViewModel @ViewModelInject constructor(
    private val todoRepository: TodoRepository
) : ViewModel() {

    fun fetchTodo(todoId: Int): LiveData<Todo?> {
        return todoRepository.fetchTodo(todoId)
    }
}