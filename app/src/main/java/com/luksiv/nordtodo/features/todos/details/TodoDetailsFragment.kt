package com.luksiv.nordtodo.features.todos.details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.luksiv.nordtodo.R
import com.luksiv.nordtodo.core.extensions.enableBackNavigation
import com.luksiv.nordtodo.data.local.todos.Todo
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.todo_details_fragment.*
import java.util.*

@AndroidEntryPoint
class TodoDetailsFragment : Fragment(R.layout.todo_details_fragment) {

    private val viewModel by viewModels<TodoDetailsViewModel>()
    private val args by navArgs<TodoDetailsFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mt_todo_details.enableBackNavigation()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.fetchTodo(args.todoId).observe(viewLifecycleOwner, ::setTodoDetails)
    }

    private fun setTodoDetails(todo: Todo?) {
        if (todo == null) {
            findNavController().navigateUp()
            return
        }

        tv_todo_details_item_title.text = todo.title

        tv_todo_details_item_status.text = if (todo.isCompleted) {
            getString(R.string.todo_status_completed)
        } else {
            getString(R.string.todo_status_not_completed)
        }

        tv_todo_details_item_updated_at.text = Date(todo.updatedAt).toString()
    }
}