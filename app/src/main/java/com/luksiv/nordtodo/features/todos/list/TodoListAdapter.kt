package com.luksiv.nordtodo.features.todos.list

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.luksiv.nordtodo.R
import com.luksiv.nordtodo.core.extensions.getString
import com.luksiv.nordtodo.core.extensions.inflate
import com.luksiv.nordtodo.core.extensions.setStrikeThrough
import com.luksiv.nordtodo.data.local.todos.Todo
import kotlinx.android.synthetic.main.view_todo_list_item.view.*

class TodoListAdapter(
    private val onTodoClicked: (todo: Todo) -> Unit
) : RecyclerView.Adapter<TodoListAdapter.TodoViewHolder>() {

    private var items: List<Todo> = emptyList()

    inner class TodoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        return TodoViewHolder(parent.inflate(R.layout.view_todo_list_item))
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        items[position].let { item ->
            with(holder.itemView) {
                tv_todo_list_item_title.text = item.title

                tv_todo_list_item_status.setStrikeThrough(item.isCompleted)

                tv_todo_list_item_status.text = if (item.isCompleted) {
                    getString(R.string.todo_status_completed)
                } else {
                    getString(R.string.todo_status_not_completed)
                }

                setOnClickListener {
                    onTodoClicked(item)
                }
            }
        }
    }

    override fun getItemCount() = items.count()

    fun updateListItems(items: List<Todo>) {
        val diffResult = DiffUtil.calculateDiff(TodoListDiffCallback(this.items, items))
        this.items = items
        diffResult.dispatchUpdatesTo(this)
    }
}