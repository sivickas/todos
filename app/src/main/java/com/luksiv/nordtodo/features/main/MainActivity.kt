package com.luksiv.nordtodo.features.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.luksiv.nordtodo.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}