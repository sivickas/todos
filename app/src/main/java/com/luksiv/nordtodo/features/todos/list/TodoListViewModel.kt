package com.luksiv.nordtodo.features.todos.list

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.luksiv.nordtodo.core.entities.SingleLiveEvent
import com.luksiv.nordtodo.core.entities.SynchronizationStatus
import com.luksiv.nordtodo.core.entities.SynchronizationStatus.SynchronizationError.*
import com.luksiv.nordtodo.core.rx.SchedulerProvider
import com.luksiv.nordtodo.data.local.todos.Todo
import com.luksiv.nordtodo.data.local.todos.TodoRepository
import com.luksiv.nordtodo.data.synchronization.TodoSynchronizationManager
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.subscribeBy
import java.net.ConnectException
import java.net.UnknownHostException

class TodoListViewModel @ViewModelInject constructor(
    private val todoRepository: TodoRepository,
    private val todoSynchronizationManager: TodoSynchronizationManager,
    private val schedulerProvider: SchedulerProvider

) : ViewModel() {

    val synchronizationStatus: SingleLiveEvent<SynchronizationStatus> = SingleLiveEvent()

    private val _todos = MutableLiveData<List<Todo>>()
    val todos: LiveData<List<Todo>> = _todos

    private val compositeDisposable = CompositeDisposable()

    init {
        synchronizeTodos()
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

    fun synchronizeTodos() {
        compositeDisposable.add(
            todoSynchronizationManager.synchronize()
                .subscribeOn(schedulerProvider.ioScheduler())
                .observeOn(schedulerProvider.mainScheduler())
                .subscribeBy(
                    onNext = {
                        synchronizationStatus.value = SynchronizationStatus.Synchronizing(it)
                    },
                    onComplete = {
                        synchronizationStatus.value = SynchronizationStatus.Completed
                        fetchTodos()
                    },
                    onError = {
                        synchronizationStatus.value = when (it) {
                            is UnknownHostException -> NoInternetError
                            is ConnectException -> InternetConnectionLostError
                            else -> UnknownError(it)
                        }
                    }
                )
        )
    }

    fun fetchTodos() {
        compositeDisposable.add(
            todoRepository
                .fetchAllTodos()
                .subscribeOn(schedulerProvider.ioScheduler())
                .observeOn(schedulerProvider.mainScheduler())
                .subscribeBy {
                    _todos.value = (it)
                }
        )
    }

}