package com.luksiv.nordtodo.features.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.updateLayoutParams
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.luksiv.nordtodo.R
import com.luksiv.nordtodo.core.extensions.runDelayed
import com.luksiv.nordtodo.features.main.MainActivity
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    companion object {
        private const val START_UP_ANIMATION_DURATION = 1000L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        cl_splash_root.postDelayed({
            TransitionManager.beginDelayedTransition(cl_splash_root, AutoTransition().also {
                it.duration = START_UP_ANIMATION_DURATION
            })

            iv_splash_logo.updateLayoutParams<ConstraintLayout.LayoutParams> {
                width = 0
            }

            runDelayed(START_UP_ANIMATION_DURATION + 200) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }, 200)
    }
}