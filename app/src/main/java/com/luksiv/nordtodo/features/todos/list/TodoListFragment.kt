package com.luksiv.nordtodo.features.todos.list

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ethanhua.skeleton.RecyclerViewSkeletonScreen
import com.ethanhua.skeleton.Skeleton
import com.luksiv.nordtodo.R
import com.luksiv.nordtodo.core.SnackbarHelper
import com.luksiv.nordtodo.core.entities.SynchronizationStatus
import com.luksiv.nordtodo.core.entities.SynchronizationStatus.Completed
import com.luksiv.nordtodo.core.entities.SynchronizationStatus.SynchronizationError.*
import com.luksiv.nordtodo.core.entities.SynchronizationStatus.Synchronizing
import com.luksiv.nordtodo.core.extensions.getColorFromTheme
import com.luksiv.nordtodo.data.local.todos.Todo
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.todo_list_fragment.*

@AndroidEntryPoint
class TodoListFragment : Fragment(R.layout.todo_list_fragment) {

    private val viewModel by viewModels<TodoListViewModel>()
    private val todoListAdapter = TodoListAdapter(::onTodoClicked)

    private lateinit var listSkeleton: RecyclerViewSkeletonScreen

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.fetchTodos()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup RecyclerView
        rv_todo_list.layoutManager = LinearLayoutManager(context)
        rv_todo_list.adapter = todoListAdapter

        // Initialize skeleton view for the rv_todo_list RecyclerView
        listSkeleton = Skeleton.bind(rv_todo_list)
            .adapter(todoListAdapter)
            .load(R.layout.view_todo_list_item_skeleton)
            .count(13)
            .show()

        // Initialize SwipeRefreshLayout
        srl_todo_refresh.setColorSchemeColors(requireContext().getColorFromTheme(R.attr.colorAccent))
        srl_todo_refresh.setOnRefreshListener {
            viewModel.synchronizeTodos()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.synchronizationStatus.observe(viewLifecycleOwner, ::updateSynchronizationStatus)
        viewModel.todos.observe(viewLifecycleOwner, ::updateListDetails)
    }

    private fun updateSynchronizationStatus(status: SynchronizationStatus) {
        when (status) {
            is Synchronizing -> {
                srl_todo_refresh.isRefreshing = true
                pb_synchronization_progress.visibility = VISIBLE
                pb_synchronization_progress.progress = status.progress
            }
            is Completed -> {
                pb_synchronization_progress.visibility = GONE
                srl_todo_refresh.isRefreshing = false
            }
            is SynchronizationStatus.SynchronizationError -> {
                pb_synchronization_progress.visibility = GONE
                srl_todo_refresh.isRefreshing = false

                val errorMessage = when (status) {
                    NoInternetError -> R.string.error_no_internet
                    InternetConnectionLostError -> R.string.error_internet_lost
                    is UnknownError -> R.string.error_unknown
                }

                SnackbarHelper.showErrorSnackbar(cl_todo_list_root, errorMessage, 4000) {
                    viewModel.synchronizeTodos()
                }
            }
        }
    }

    private fun updateListDetails(items: List<Todo>) {
        // Hide the skeleton view if items are present
        if (items.isNotEmpty()) {
            listSkeleton.hide()
        }

        // Update item count
        mt_todo_list.title = getString(R.string.todo_list_title) + " (${items.count()})"

        // Update the items in the adapter
        todoListAdapter.updateListItems(items)
    }

    private fun onTodoClicked(todo: Todo) {
        // Navigate to To-do details fragment
        findNavController().navigate(
            TodoListFragmentDirections.actionTodoListFragmentToTodoDetailsFragment(todo.id)
        )
    }
}